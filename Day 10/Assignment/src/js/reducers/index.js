import { combineReducers } from "redux";
import counterReducer from './counterReducer'
import postsReducer from './postsReducer'
import formReducer from './formReducer'
import employeeReducer from './employeeReducer'

const rootReducer = combineReducers({
    counterReducer,
    postsReducer,
    formReducer,
    employeeReducer
})
export default rootReducer;