import './App.css';
import  Counter  from './components/Counter';
import ListForm from './components/ListForm';
import List from './components/list';
import Post from './components/Post';
import EmployeeForm from './components/EmployeeForm';
import Table from './components/Table';

function App() {
  return (
    <div>
        <Counter />
        <Post />
        <List />
        <ListForm />
        <Table />
        <EmployeeForm />
    </div>
  );
}

export default App;
