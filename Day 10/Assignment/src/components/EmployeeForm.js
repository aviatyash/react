import React from "react";
import { useState } from "react";
import { useDispatch} from "react-redux";
import { SaveData } from "../js/actions/index";

function EmployeeForm() {
    const dispatch= useDispatch();
    const [name,setName]=useState('');
    const [id,setId]=useState();
    const [age,setAge]=useState();
    const [gender,setGender]=useState();
    const [company,setCompany]=useState('');
    const submitHandler=(event)=>{
       event.preventDefault();
        let data={
            name,
            id,
            age,
            gender,
            company
        };
        dispatch(SaveData(data));
    }
    return ( <div style={{width:'400px'}} className="container">
       <form className="form-inline">
       <div className="form-group row">
            <input type="text" className="form-control" onChange={(e)=>setId(e.target.value)} placeholder='Emp id' />
        </div>
        <div className="form-group row">
            <input type="text" className="form-control" onChange={(e)=>setName(e.target.value)} placeholder='Name' />
        </div>
        <div className="form-group row mb-2">
            <input type="number" className="form-control" onChange={(e)=>setAge(e.target.value)} placeholder='Age' />
        </div>
        <div className="form-check" onChange={(e)=>setGender(e.target.value)}>
            <label>Gender :</label> {" "}
            <input type="radio" value="Male" name="gender" /> Male {" "}
            <input type="radio" value="Female" name="gender"  /> Female
        </div>
        <div className="form-group row">
            <input type="text" className="form-control" onChange={(e)=>setCompany(e.target.value)} placeholder='Company Name' />
        </div>
        <button  onClick={submitHandler} type="submit" className="btn btn-success"> Save </button>{" "}
        <button className="btn btn-secondary" type="reset">Reset</button>
       </form>
        
   
    </div> );
}

export default EmployeeForm;