import { useSelector } from "react-redux";

function Table() {
    const data= useSelector((state)=> state.data);
    return ( 
        <div>
            <table>
                <thead >
                    <tr >
                        <th>Emp Id</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Company</th>
                    </tr>
                </thead>
                <tbody>
                       {
                    data?.map((data,i)=>(
                        <tr key={i}>
                            <th>{data.id}</th>
                            <td>{data.name}</td>
                            <td>{data.age}</td>
                            <td>{data.gender}</td>   
                            <td>{data.company}</td>
                       </tr>
                    ))
                    
                    }
                </tbody>
            </table>
        </div>
     );
}

export default Table;