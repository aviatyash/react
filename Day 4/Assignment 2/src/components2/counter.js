import React from "react";
class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }
    static getDerivedSateFromProps(props,state){
        console.log("getDerivedStateFromProps is called",state.count)
        return null;
    }
       componentDidMount(){
        console.log("component did mount is called");
    }
    shouldComponentUpdate(){
        console.log("shouldComponentUpdate is called");
        return true;
    }
    getSnapshotBeforeUpdate(prevProps,prevState){
        console.log("getSnapshotBeforeUpdate called")
        return null;
    }
    componentDidUpdate(){
        console.log("componentDidUpdate called");
    }
    componentWillUnmount(){
        console.log("componentWillUnmount Called.")
    }

  render() {
      console.log("render is called");
    return (
      <div>
        <p>You clicked {this.state.count} times</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}
export default Counter;
