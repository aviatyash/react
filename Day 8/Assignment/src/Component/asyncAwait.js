import React, { Component } from "react";
import axios from "axios";

class AsyncAwait extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }
  componentDidMount() {
    this.getUsers();
  }
  async getUsers() {
    let res = await axios.get(`https://jsonplaceholder.typicode.com/users`);
    this.setState({ data: res.data });
    console.log(res.data);
    console.log(this.state);
  }
  render() {
    return (
      <div>
        <div className="container">
          <h4>Users</h4>
          <hr></hr>
          <div className="container">
            <table
              className="table table-bordered"
              style={{ width: "100%", border: "1px solid #ccc" }}
            >
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Username</th>
                  <th>Website</th>
                </tr>
              </thead>
              <tbody>
                {this.state.data.map((item, i) => (
                  <tr key={i}>
                    <td>{item.id}</td>
                    <td>{item.username}</td>
                    <td>{item.website}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default AsyncAwait;
