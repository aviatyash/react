import './App.css';
import React from 'react';
import AsyncAwait from './Component/asyncAwait';
import Promises from './Component/promise';


function App() {
  return (
    <div className="App">
    <AsyncAwait></AsyncAwait>
    <Promises></Promises>
    </div>
  );
}

export default App;
