function Table({tableData}){
   const spliceRow=()=>{
       let a=document.getElementById('td1').innerHTML;
       console.log(a)
       document.getElementById('mytable').deleteRow(a)

    }

    return(
        <table id='mytable' className="table">
            <thead>
                <tr>
                    <th>S.N</th>
                    <th>Full Name</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                </tr>
            </thead>
            <tbody>
            {
                tableData.map((data, index)=>{
                    return(
                        <tr key={index+1}>
                            <td>{index}</td>
                            <td>{data.fullName}</td>
                            <td>{data.dob}</td>
                            <td>{data.gender}</td>
                            <td><button onClick={()=>spliceRow()}>X</button></td>
                        </tr>
                    )
                })
            }
            </tbody>
        </table>
    )
}

export default Table;