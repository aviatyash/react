function FormInput({handleChange, formInputData, handleSubmit}){
return(
    <div className="form-row row">
      <div className="col">
          <label>Name</label>
        <input type="text" onChange={handleChange} value={formInputData.fullName} name="fullName" className="form-control"  placeholder="Full Name" />
      </div>
      <div className="col">
          <label>Date of Birth</label>
        <input type="date" onChange={handleChange} value={formInputData.dob} name="dob" className="form-control"/>
      </div>
      <div className="col">
          <label>Gender</label>
        <input type="text" onChange={handleChange} value={formInputData.gender} name="gender" className="form-control" placeholder="male" />
      </div>
      <div className="col">
        <input type="submit" onClick={handleSubmit} className="btn btn-primary" />
      </div>
    </div>
 
  
)
}
export default FormInput;