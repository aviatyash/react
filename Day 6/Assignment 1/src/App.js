
import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './components/DisplayFormDataInTable';
import AddDeleteTableRows from './components/AddDeleteTableRows';


function App() {
  return (
    <div className="App">
      <AddDeleteTableRows></AddDeleteTableRows>
    </div>
  );
}

export default App;
