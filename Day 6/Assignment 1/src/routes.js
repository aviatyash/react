import React, { Component } from "react";
import Switch from 'react-router-dom';
import { Route } from "react-router-dom";
import Users from "./componants/users";
import Contact from "./componants/contact";
import About from "./componants/about";
import NoMatch from "./componants/nomatch";
import Home from "./componants/home";

export default class Routes extends Component{
    render(){
        return(
            <Routes>
                <Route component={Home} exact path="/"/>
                <Route component={Users} exact path="/users"/>
                <Route component={About} exact path="/aboutus"/>
                <Route component={Contact} exact path="/contact"/>
                <Route component={NoMatch} path="*"/>
            </Routes>
        )
    }
}
