import React from 'react';
import axios from 'axios';
import { useEffect } from 'react';
import { useState } from 'react';
const GetListFunction=()=>{
    const [APIData,setAPIData]=useState([]);
    const [empname,setname]=useState('');
    const [empsal,setsal]=useState('');
    const [empage,setage]=useState('');
    
     useEffect(() => {
         document.getElementById('div').style.display='none'
        axios.get(`https://62596bffcda73d132d150e9e.mockapi.io/React6`)
            .then((response) => {
                setAPIData(response.data);
            })
    }, [])
    const  deletehandle=()=>{
    let a=document.getElementById('id').innerHTML
    console.log(a);
    axios.delete(`https://62596bffcda73d132d150e9e.mockapi.io/React6/${a}`)
    console.log(`Deleted Successfully`)
  }
   const namechange=(event)=>{
       setname(event.target.value);
       console.log(empname)
    }
    const salchange=(event)=>{
       setsal(event.target.value);
       console.log(empsal)
    }
    const salage=(event)=>{
       setage(event.target.value);
       console.log(empage)
    }
    const handlesubmit=(e)=>{
        e.preventDefault();
        let id=document.getElementById('id').innerHTML
        console.log(id);
        axios.put(`https://62596bffcda73d132d150e9e.mockapi.io/React6/${id}`,{
        empname,
        empsal,
        empage
    });
    setname('');
    setsal('');
    setage('')
}
   const edithandle=()=>{
    document.getElementById('mydiv').style.display='none';
    document.getElementById('div').style.display=''
    
}

    return(
        <>
          <div id='mydiv'>
                <h1> Employee Data</h1>{
                APIData.map((item) => ( 
                <ol key = { item.id } >
                  <p id='id'>{item.id}</p>
                    Employee Name:  { item.empname }<br/>
                    Employee salary: {item.empsal}<br/>
                    Employee Age:  {item.empage}<br/>
                    <button onClick={deletehandle}>Delete</button>
                    <button onClick={edithandle}>Edit</button><br/>
                    </ol>
                ))
            }
          </div>
          <div id='div'>
                <form onSubmit={handlesubmit}>
            <label htmlFor="empname">Employee Name</label>
            <input type='text' id='empname' onChange={namechange} value={empname}></input><br/>
            <label htmlFor="empsal ">Salary</label>
            <input type='text' id='empsal' onChange={salchange} value={empsal}></input><br/>
            <label htmlFor="empage">Age</label>
            <input type={'text'} id='empage' onChange={salage} value={empage}></input><br/>
            <button type="submit">Submit</button>
        </form> 
          </div>

          </>

    )
        }

export default GetListFunction;