import axios from 'axios';
import React from 'react';
class GetList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            items:[],
            isEdit:true,
        };
    }
componentDidMount() {
      fetch("https://62596bffcda73d132d150e9e.mockapi.io/React6")
        .then(res => res.json())
        .then(result =>
          this.setState({
            items: result
          })
        )
        .catch(console.log);
  }
  deletehandle=()=>{
    let a=document.getElementById('id').innerHTML
    console.log(a);
    axios.delete(`https://62596bffcda73d132d150e9e.mockapi.io/React6/${a}`)
    console.log(`Deleted Successfully`)

  }
  edithandle=()=>{
    let b =document.getElementById('id').innerHTML;
    this.state.isEdit=false;
    
}
  render(){
      const items=this.state.items;
      return(
        <>
          <div>
                <h1> Employee Data </h1>{
                items.map((item) => ( 
                <ol key = { item.id } >
                  <p id='id'>{item.id}</p>
                    Employee Name:  { item.empname }<br/>
                    Employee salary: {item.empsal}<br/>
                    Employee Age:  {item.empage}<br/>
                    <button onClick={this.deletehandle}>Delete</button>
                    <button onClick={this.edithandle}>Edit</button><br/>
                    </ol>
                ))
            }
          </div>

          </>
          
      )
  }
}
export default GetList