import './App.css';
import CreateList from './components/createlist';
import GetList from './components/getlist';
import { BrowserRouter as Router,Routes,Route,Link } from 'react-router-dom';
import GetListFunction from './components/getlistfunction';

function App() {
  return (
   <Router>
     <div className='App'>
       <Link to='/createlist'>
       </Link>
       <ul>
         <li>
           <Link to='/getlist'>Get List</Link>
         </li>
         <li>
           <Link to='createlist'>Create List</Link>
         </li>
       </ul>
     </div>
     <Routes>
       <Route exact path='/' element={<CreateList/>}></Route>
       <Route exact path='/createlist' element={<CreateList/>}></Route>
       <Route exact path='/getlist' element={<GetListFunction/>}></Route>
     </Routes>

   </Router>
  );
}

export default App;
