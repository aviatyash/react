import { useState } from "react";
import TableRowsstylesheets from "./tableroestylesheet";
import './addtablerow.css'
function AddDeleteTableRowsstylesheets(){


    const [rowsData, setRowsData] = useState([]);
 
    const addTableRows = ()=>{
  
        const rowsInput={
            fullName:'',
            emailAddress:'',
            salary:''  
        } 
        setRowsData([...rowsData, rowsInput])
      
    }
   const deleteTableRows = (index)=>{
       console.log(index)
        const rows = [...rowsData];
        rows.splice(index, 1);
        setRowsData(rows);
   }
 
   const handleChange = (index, evnt)=>{
    
    const { name, value } = evnt.target;
    const rowsInput = [...rowsData];
    rowsInput[index][name] = value;
    setRowsData(rowsInput);
  
 
 
}
    return(
        <div>
            <div>
                <div>

                <table style={{border:"1px solid",width:"100%"}}>
                    <thead>
                      <tr>
                          <th>Full Name</th>
                          <th>Date of Birth</th>
                          <th>Gender</th>
                          <th><button onClick={addTableRows} >+</button></th>
                      </tr>

                    </thead>
                   <tbody>
                       <TableRowsstylesheets rowsData={rowsData} deleteTableRows={deleteTableRows} handleChange={handleChange} ></TableRowsstylesheets>

                   </tbody> 
                </table>

                </div>
                <div>

                </div>
            </div>
        </div>
    )

}
export default AddDeleteTableRowsstylesheets