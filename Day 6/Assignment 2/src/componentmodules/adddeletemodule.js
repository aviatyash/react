import { useState } from "react";
import TableRowsmodule from "./tablerowmodule";
import style from './adddeletemodule.module.css';
function AddDeleteTableRowsModule(){


    const [rowsData, setRowsData] = useState([]);
 
    const addTableRows = ()=>{
  
        const rowsInput={
            fullName:'',
            emailAddress:'',
            salary:''  
        } 
        setRowsData([...rowsData, rowsInput])
      
    }
   const deleteTableRows = (index)=>{
       console.log(index)
        const rows = [...rowsData];
        rows.splice(index, 1);
        setRowsData(rows);
   }
 
   const handleChange = (index, evnt)=>{
    
    const { name, value } = evnt.target;
    const rowsInput = [...rowsData];
    rowsInput[index][name] = value;
    setRowsData(rowsInput);
  
 
 
}
    return(
        <div>
            <div>
                <div>

                <table className={style.table}>
                    <thead>
                      <tr>
                          <th>Full Name</th>
                          <th>Date of Birth</th>
                          <th>Gender</th>
                          <th><button onClick={addTableRows} className={style.button}>+</button></th>
                      </tr>

                    </thead>
                   <tbody>
                       <TableRowsmodule rowsData={rowsData} deleteTableRows={deleteTableRows} handleChange={handleChange} ></TableRowsmodule>

                   </tbody> 
                </table>

                </div>
                <div>

                </div>
            </div>
        </div>
    )

}
export default AddDeleteTableRowsModule;