import './App.css';
import AddDeleteTableRowsModule from './componentmodules/adddeletemodule';
import AddDeleteTableRows from './componentsinline/AddDeleteTableRows';
import AddDeleteTableRowsstylesheets from './componentsstylesheets/Adddeletetablestylesheets';

function App() {
  return (
    <div>
      <AddDeleteTableRowsModule></AddDeleteTableRowsModule>
    </div>
  );
}

export default App;
