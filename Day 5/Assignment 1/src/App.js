
import './App.css';
import {
	BrowserRouter as Router,
	Routes,
	Route,
	Link
} from 'react-router-dom';
import Animals from './components/animals';
import LivingThings from './components/livingthings';
import Birds from './components/birds';
import Employees from './components/employees';
import Users from './components/users';


function App() {
  return (
    <Router>
    <div className="App">
      <Link to="/"></Link>
	<ul>
		
	<li>
		<Link to="/animals">Animals</Link>
	</li>
	<li>
		<Link to="/birds">Birds</Link>
	</li>
  <li>
		<Link to="/employees">Employees</Link>
	</li>
  <li>
		<Link to="/users">Users</Link>
	</li>
	</ul>
</div>
<Routes>
  <Route exact path='/' element={< LivingThings />}></Route>
<Route exact path='/animals' element={< Animals />}></Route>
<Route exact path='/birds' element={< Birds />}></Route>
<Route exact path='/employees' element={< Employees />}></Route>
<Route exact path='/users' element={< Users />}></Route>
</Routes>
</Router>
  );
}

export default App;
