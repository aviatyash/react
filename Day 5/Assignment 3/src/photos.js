import React, { useState } from "react";
import { useEffect } from "react";
function Photoscomponent(){
    const[data,setData]=useState([]);
     useEffect(()=>{
        console.log("UseEffect Once at intial load only");
        fetch("https://picsum.photos/v2/list")
        .then((response)=>response.json())
        .then((data)=>setData(data));
    },[]);

    return(
        <div>
            <h2>Photos From Internet</h2>
            {
                data.map((item)=>(
                    <ol key={item.id}>
                        <p><img src={item.url} alt={item.id}></img></p>
                    </ol>
                ))
            }

        </div>

    )


}
export default Photoscomponent;