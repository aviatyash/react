import React,{useState,useEffect} from "react";
function Employeescomponent(){
    const[data,setData]=useState([]);
    useEffect(()=>{
        console.log("UseEffect Once at intial load only");
        fetch("http://dummy.restapiexample.com/api/v1/employees")
        .then((response)=>response.json())
        .then((data)=>setData(data.data));
    },[]);

    return(
        <div className="container">
            <hr />
            <div className="container">
            <table className="tabel table-bordered" style={{width:"100%",border:"1px solid #CCC"}}>
        <thead>
            <tr>
                <th>Employee Name</th>
                <th>Employee Salary</th>
                <th>Employee Age</th>
            </tr>
        </thead>
        <tbody>
            {data.map((items,index)=>(
                <tr key={index}>
                    <td>{items.employee_name}</td>
                    <td>{items.employee_salary}</td>
                    <td>{items.employee_age}</td>
                </tr>
            ))}
        </tbody>
    </table>
            </div>
        </div>
    )
}
export default Employeescomponent;