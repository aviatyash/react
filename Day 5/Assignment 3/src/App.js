import './App.css';
import { BrowserRouter as Router,Routes,Route,Link } from 'react-router-dom';
import Home from './home';
import Employeescomponent from './employees';
import Photoscomponent from './photos'

function App() {
  return (
      <Router>
         <div className="App">
           <ul>
             <li>
             <Link to='/employees'>Employees</Link>
             </li>
             <li>
             <Link to='/photos'>Photos</Link>
             </li>
           </ul>
         </div>
         <Routes>
           <Route exact path='/' element={<Home></Home>}></Route>
           <Route exact path='/employees' element={<Employeescomponent></Employeescomponent>}></Route>
             <Route exact path='/photos' element={<Photoscomponent></Photoscomponent>}></Route>
           
         </Routes>
      </Router>
  );
}

export default App;
