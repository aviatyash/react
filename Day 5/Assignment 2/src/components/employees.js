import React, { Component } from 'react'
class Employees extends Component{
    constructor(props){
        super(props);
        this.state={
            name:"Employees",
            info:"This is a stateful class componant"
        }
    }
    render(){
        return(
            <div>
                <h2>{this.state.name}</h2>
                <p>{this.state.info}</p>
            </div>
        )
    }
}
export default Employees;