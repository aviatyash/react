import React,{useState} from "react";
import AnimalProps from "./animalprops";
class Animals extends React.Component {
    constructor(props){
        super(props);
        this.state={
            animaldata:[
                {
                    name:"",
                    animal:"",
                }
            ],
        }
    }
    changeName=(data)=>{
        this.setState({
            animaldata:data,
        })
    }
    render(){
        return(
            <>
            <AnimalProps changeNameAsProps={this.changeName}/>
            <div>
                {this.state.animaldata.map((items,index)=>(
                    <div>
                   <p>{items.name}</p> 
                   <p>{items.info}</p> 
                   </div>
                ))}
            </div>
            </>
        )
    }

}
export default Animals;