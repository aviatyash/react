import React,{useState} from "react";
import BirdProps from "./birdsprops";
class Birds extends React.Component {
    constructor(props){
        super(props);
        this.state={
            birddata:[
                {
                    name:"",
                    animal:"",
                }
            ],
        }
    }
    changeName=(data)=>{
        this.setState({
            birddata:data,
        })
    }
    render(){
        return(
            <>
            <BirdProps changeNameAsProps={this.changeName}></BirdProps>
            <div>
                {this.state.birddata.map((items,index)=>(
                    <div>
                   <p>{items.name}</p> 
                   <p>{items.info}</p> 
                   </div>
                ))}
            </div>
            </>
        )
    }

}
export default Birds;