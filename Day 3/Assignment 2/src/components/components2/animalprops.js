import React from "react";
const AnimalProps=props=>{
    const data=[
        {
            name:"Animal",
            info:"This is a component about animals",

        }
    ]
    return(
        <div>
            <button onClick={()=>props.changeNameAsProps(data)}>Animals as props</button>
        </div>
    )
}
export default AnimalProps;