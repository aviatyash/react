import React from "react";
const BirdProps=props=>{
    const data=[
        {
            name:"Birds",
            info:"This is a component about Birds",

        }
    ]
    return(
        <div>
            <button onClick={()=>props.changeNameAsProps(data)}>Birds as props</button>
        </div>
    )
}
export default BirdProps;