import React,{useState} from "react";
import UserProps from "./userprops";
class Users extends React.Component {
    constructor(props){
        super(props);
        this.state={
            userdata:[
                {
                    name:"",
                    animal:"",
                }
            ],
        }
    }
    changeName=(data)=>{
        this.setState({
            userdata:data,
        })
    }
    render(){
        return(
            <>
            <UserProps changeNameAsProps={this.changeName}></UserProps>
            <div>
                {this.state.userdata.map((items,index)=>(
                    <div>
                   <p>{items.name}</p> 
                   <p>{items.info}</p> 
                   </div>
                ))}
            </div>
            </>
        )
    }

}
export default Users;