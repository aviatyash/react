import React,{useState} from "react";
import EmployeesProps from "../employeesprops";
class Employees extends React.Component {
    constructor(props){
        super(props);
        this.state={
            employeedata:[
                {
                    name:"",
                    animal:"",
                }
            ],
        }
    }
    changeName=(data)=>{
        this.setState({
            employeedata:data,
        })
    }
    render(){
        return(
            <>
            <EmployeesProps changeNameAsProps={this.changeName}></EmployeesProps>
            <div>
                {this.state.employeedata.map((items,index)=>(
                    <div>
                   <p>{items.name}</p> 
                   <p>{items.info}</p> 
                   </div>
                ))}
            </div>
            </>
        )
    }

}
export default Employees;