import React from "react";
const EmployeesProps=props=>{
    const data=[
        {
            name:"Employees",
            info:"This is a component about employees",

        }
    ]
    return(
        <div>
            <button onClick={()=>props.changeNameAsProps(data)}>Employees as props</button>
        </div>
    )
}
export default EmployeesProps;