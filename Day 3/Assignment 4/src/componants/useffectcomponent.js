import React,{useState,useEffect} from "react";
function UseEffectcomponent(){
    const[data,setData]=useState([]);
    useEffect(()=>{
        console.log("UseEffect Once at intial load only");
        fetch("https://jsonplaceholder.typicode.com/users")
        .then((response)=>response.json())
        .then((data)=>setData(data))
    },[]);

    return(
        <div className="container">
            <div className="container">
            <table className="tabel table-bordered" style={{width:"100%",border:"1px solid #CCC"}}>
        <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Address</th>
                <th>Geo Location</th>
                <th>Phone</th>
                <th>Website</th>
                <th>Company</th>
                
            </tr>
        </thead>
        <tbody>
            {data.map((items,index)=>(
                <tr key={index}>
                    <td>{items.name}</td>
                    <td>{items.username}</td>
                    <td>{items.email}</td>
                    <td>{items.address.street},{items.address.suite},{items.address.city},{items.address.zipcode}</td>
                    <td>{items.address.geo.lat},{items.address.geo.lng}</td>
                    <td>{items.phone}</td>
                    <td>{items.website}</td>
                    <td>{items.company.name}</td>
                

                </tr>
            ))}
        </tbody>
    </table>
            </div>
        </div>
    )
}
export default UseEffectcomponent;