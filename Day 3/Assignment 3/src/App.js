import './App.css';
import Animals from './components/components2/animals';
import Birds from './components/components2/birds';
import Employees from './components/components2/employees';
import Users from './components/components2/users';


function App() {
  return (
    <div className="App">
      <Animals></Animals>
      <Birds></Birds>
      <Employees></Employees>
      <Users></Users>
     
    </div>
  );
}

export default App;
