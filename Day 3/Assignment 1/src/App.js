
import './App.css';
import Animals from './components/components2/animals';
import Birds from './components/components2/birds';
import Employees from './components/components2/employees';
import LivingThings from './components/components2/livingthings';
import Users from './components/components2/users';


function App() {
  return (
    <div className="App">
      <Animals></Animals>
      <Users></Users>
      <Employees></Employees>
      <Birds></Birds>
      <LivingThings></LivingThings>
    </div>
  );
}

export default App;
