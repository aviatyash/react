import React,{useState} from "react";
const Birds=({})=>{
    const [componentNAme,setComponentName]=useState("______");
    const [componentinfo,setComponentinfo]=useState('')
    return(
        <>
        <h3>UseState Hooks</h3>
        <h2>This is {componentNAme} component</h2>
        <p>{componentinfo}</p>
        <button onClick={()=>setComponentName("Birds")}>component Name</button>
        <button onClick={()=>setComponentinfo("This is component info")}>Component Info</button>
        <hr></hr>
        </>
    )
}
export default Birds;