import React,{useState} from "react";
const Animals=({})=>{
    const [componentNAme,setComponentName]=useState("______");
    const [componentinfo,setComponentinfo]=useState('')
    return(
        <>
        <h3>UseState Hooks Animals</h3>
        <h2>This is {componentNAme} component</h2>
        <p>{componentinfo}</p>
        <button onClick={()=>setComponentName("Animal")}>component Name</button>
        <button onClick={()=>setComponentinfo("This is component info")}>Component Info</button>
        </>
    )
}
export default Animals;