import './App.css';
import UserDataFunction from './components/usertablefunction';

const UserData=[
  {
      name:"Vijaya",
      age:"20",
      gender:"female",
      id:"111",
    },
    {
      name:"Smita",
      age:"25",
      gender:"female",
      id:"222",
    },
    {
      name:"Aryan",
      gender:"male",
      age:"23",
      id:"333",
    },
    {
      name:"Abhay",
      gender:"male",
      age:"27",
      id:"444",
    },
]

function App() {
  return (
    <div className="App">
      <UserDataFunction userList={UserData}></UserDataFunction>

    </div>
  );
}

export default App;
