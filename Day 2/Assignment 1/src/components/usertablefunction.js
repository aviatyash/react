import React from "react";
const UserDataFunction=({userList})=>{
    return(
        <>
         <p>User Table Using Function Component</p>
        <div>
            <table style={{background:"blue",color:"white"}}>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>ID</th>
                    </tr>
                </thead>
                <tbody>
                    {userList.map((items,index)=>(
                        <tr key={index}>
                            <td>{items.name}</td>
                            <td>{items.gender}</td>
                            <td>{items.age}</td>
                            <td>{items.id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
        </>
    )
}
export default UserDataFunction;
