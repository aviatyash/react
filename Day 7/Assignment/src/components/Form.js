import React, { Component } from "react";
import { connect } from "react-redux";
import { addArticle } from "../js/actions/index";

class ConnectedForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            emp_id: "",
            emp_name:"",
            emp_age:"",
            emp_gender:"",
            emp_company:""
           

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ 
            [event.target.name]:event.target.value,
        });
        
    }

    handleSubmit(event) {
        event.preventDefault();
       
        this.props.addArticle(this.state);
        this.setState({ emp_id: "" ,emp_name:"",emp_age:"",emp_gender:"",emp_company:""});

       
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit} style={{ width: 400 + 'px' }}>
                <div className='form-group'>
                    
                    <input
                        type="number"
                        id="emp_id"
                        name="emp_id"
                        placeholder="ID"
                        value={this.state.emp_id}
                        onChange={this.handleChange}
                        className="form-control"
                    />
                    <br />
                    <br />


                    <input
                        type="text"
                        id="emp_name"
                        name="emp_name"
                        placeholder="Name"
                        value={this.state.emp_name}
                        onChange={this.handleChange}
                        className="form-control"
                    />
                         <br />
                    <br />
                    

                    <input
                        type="number"
                        id="emp_age"
                        name="emp_age"
                        placeholder="Age"
                        value={this.state.emp_age}
                        onChange={this.handleChange}
                        className="form-control"
                    />
                     <br />
                    <br />

 
                    <input
                        type="text"
                        id="emp_gender"
                        name="emp_gender"
                        placeholder="Gender"
                        value={this.state.emp_gender}
                        onChange={this.handleChange}
                        className="form-control"
                    />
                     <br />
                    <br />

                    <input
                        type="text"
                        id="emp_company"
                        name="emp_company"
                        placeholder="Company"
                        value={this.state.emp_company}
                        onChange={this.handleChange}
                        className="form-control"
                    />
                     <br />
                    <br />




                    

                </div>
                <button className='btn btn-primary' type="submit">SAVE</button>
            </form>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return {
        addArticle: article => dispatch(addArticle(article))
    };
}

const Form = connect(
    null,
    mapDispatchToProps
)(ConnectedForm);

export default Form;