import React from "react";
import { Table } from "react-bootstrap";
import { connect } from "react-redux";

const ConnectedList = (props) => (
   
   <Table>
      <thead>
      <tr>
          <th>Employee Id</th>
          <th> Name</th>
          <th>Age</th>
          <th>Gender</th>
          <th>Company</th>
      </tr>
      </thead>
      <tbody>
    
      {props.articles.map((item, index) => (
            <tr key={index}>
                <td>{item.emp_id}</td>
                <td>{item.emp_name}</td>
                <td>{item.emp_age}</td>
                <td>{item.emp_gender}</td>
                <td>{item.emp_company}</td>

            </tr>
           
        ))}
          
     
       </tbody>
        </Table>

    
);

const mapStateToProps = state => {
    return { articles: state.articles };
};

const List = connect(mapStateToProps, null)(ConnectedList);

export default List;