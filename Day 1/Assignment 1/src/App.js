import logo from './logo.svg';
import './App.css';
import AnimalkingdomClass from './components/Animalkingdomclass';

function App() {
  return (
    <div className="App">
     <AnimalkingdomClass></AnimalkingdomClass>
    </div>
  );
}

export default App;
