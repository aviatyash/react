import React from "react";
class LionClass extends React.Component {
    render(){
        return(
            <>
            <h3>LION</h3>
            <p>The lion (Panthera leo) is a large cat of the genus Panthera native to Africa and India.
            It has a muscular, deep-chested body, short, rounded head, round ears, and a hairy tuft at the end of its tail</p>
            </>
        )
    }
}
export default LionClass;