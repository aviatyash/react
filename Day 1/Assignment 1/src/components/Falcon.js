import React from "react";
const Falcon=()=>{
    return(
        <>
        <h3>Falcon</h3>
        <p>Falcons are birds of prey in the genus Falco,
             which includes about 40 species. Falcons are widely distributed on all continents of the world except Antarctica, though closely related raptors did occur there in the Eocene.</p>
        </>
    )
}
export default Falcon;