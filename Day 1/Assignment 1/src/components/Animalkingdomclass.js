import React from "react";
import DholeClass from "./Dholeclass";
import Falcon from "./Falcon";
import GaurClass from "./Gaurclass";
import GrizzlyClass from "./Grizzlyclass";
import JaguarClass from "./Jaguarclass";
import Kingfisher from "./Kingfisher";
import Kookaburras from "./kookaburafunction";
import Ostrich from "./kookaburafunction";
import LionClass from "./Lionclass";
import Peafowl from "./Peafowl";
import Shoebill from "./Shoebillfunction";
import TigerClass from "./Tigerclass";
class AnimalkingdomClass extends React.Component {
    render(){
        return(
            <>
            <TigerClass></TigerClass>
            <LionClass></LionClass>
            <JaguarClass></JaguarClass>
            <DholeClass></DholeClass>
            <GrizzlyClass></GrizzlyClass>
            <GaurClass></GaurClass>
            <Shoebill></Shoebill>
            <Kookaburras></Kookaburras>
            <Peafowl></Peafowl>
            <Ostrich></Ostrich>
            <Kingfisher></Kingfisher>
            <Falcon></Falcon>
            </>
        )
    }
}
export default AnimalkingdomClass;