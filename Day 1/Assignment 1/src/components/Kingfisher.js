import React from "react";
const Kingfisher=()=>{
    return(
        <>
        <h3>Kingfisher</h3>
        <p>Kingfishers or Alcedinidae are a family of small to medium-sized, brightly colored birds in the order Coraciiformes. They have a cosmopolitan distribution,
             with most species found in the tropical regions of Africa, Asia, and Oceania but also can be seen in Europe</p>
        </>
    )
}
export default Kingfisher;