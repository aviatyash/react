import React from "react";
const Ostrich=()=>{
    return(
        <>
        <h3>Ostrich</h3>
        <p>There are two living species of ostrich: the common ostrich and the Somali ostrich.
             They are large flightless birds of Africa who lay the largest eggs of any living land animal.
              With the ability to run at 70 km/h (43.5 mph), they are the fastest birds on land.</p>
        </>
    )
}
export default Ostrich;