import React from "react";
const Kookaburras=()=>{
    return(
        <>
        <h3>Kookaburras</h3>
        <p>Kookaburras are terrestrial tree kingfishers of the genus Dacelo native to Australia and New Guinea, which grow to between 28 and 47 cm (11 and 19 in) in length and weigh around 300 g (11 oz).
             The name is a loanword from Wiradjuri guuguubarra, onomatopoeic of its call</p>
        </>
    )
}
export default Kookaburras;