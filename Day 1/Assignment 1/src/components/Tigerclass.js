import React from "react";
class TigerClass extends React.Component {
    render(){
        return(
            <>
            <h3>TIGER</h3>
            <p>The tiger (Panthera tigris) is the largest living cat species and a member of the genus Panthera.
                 It is most recognisable for its dark vertical stripes on orange fur with a white underside</p>
            </>
        )
    }
}
export default TigerClass;