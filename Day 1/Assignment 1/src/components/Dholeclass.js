import React from "react";
class DholeClass extends React.Component {
    render(){
        return(
            <>
            <h3>Dhole</h3>
            <p>The dhole (/doʊl/; Cuon alpinus) is a canid native to Central, South, East, and Southeast Asia.
                 Other English names for the species include Asian wild dog, Asiatic wild dog, Indian wild dog, whistling dog, red dog, and mountain wolf</p>
            </>
        )
    }
}
export default DholeClass;